package org.catness.ijpost

import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.util.TypedValue
import android.view.*
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.preference.PreferenceManager
import org.catness.ijpost.databinding.FragmentTitleBinding
import java.lang.Exception
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.Period
import java.time.format.DateTimeFormatter
import java.util.*

class TitleFragment : Fragment(), SharedPreferences.OnSharedPreferenceChangeListener {
    private val LOG_TAG: String = this.javaClass.simpleName
    private lateinit var binding: FragmentTitleBinding
    private val textSizeDefault: Float = 18f
    private lateinit var pref: SharedPreferences

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_title, container, false
        )
        binding.title.setText(getDate())
        pref = PreferenceManager.getDefaultSharedPreferences(context)
        pref.registerOnSharedPreferenceChangeListener(this)

        if (pref.contains("textSize")) {
            var size =
                pref.getString("textSize", textSizeDefault.toString())?.toFloatOrNull()
                    ?: textSizeDefault
            updateTextSize(size)
        }
        if (pref.contains("content")) {
            val content: String = pref.getString("content", "") ?: ""
            if (content != "") {
                binding.content.setText(content)
                binding.content.setSelection(binding.content.length())
            }
            else initPost()
        }

        val mood: InstantAutoCompleteTextView = binding.mood
// Create an ArrayAdapter using the string array and a default spinner layout
        activity?.let {
            ArrayAdapter.createFromResource(
                it,
                R.array.moods_array,
                android.R.layout.simple_spinner_item
            ).also { adapter ->
                // Specify the layout to use when the list of choices appears
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                // Apply the adapter to the spinner
                mood.setAdapter(adapter)
            }
        }
        setHasOptionsMenu(true)
        return binding.root
    }

    fun initPost() {
        val useTemplate = pref.getBoolean("useTemplate", false)
        Log.i(LOG_TAG,"initPost: use template: $useTemplate")
        if(useTemplate){
            val template: String = pref.getString("template", "")?:""
            Log.i(LOG_TAG,template)
            binding.content.setText(template)
        }
        else {
            Log.i(LOG_TAG,"clear text ...")
            binding.content.text.clear()
        }
        binding.content.setSelection(0)
    }

    fun getDate(): String {
        val formatter = DateTimeFormatter.ofPattern("E, dd MMM yyyy")
        var dateTime = LocalDateTime.now()
        dateTime = dateTime.minusHours(4)
        val currentDate = dateTime.format(formatter)
        return currentDate
    }

    fun openSettings(): Boolean {
        //Toast.makeText(context, "open settings", Toast.LENGTH_LONG).show()
        activity?.let {
            val intent = Intent(it, SettingsActivity::class.java)
            it.startActivity(intent)
        }
        return true
    }

    fun String.isEmailValid(): Boolean {
        return !TextUtils.isEmpty(this) && android.util.Patterns.EMAIL_ADDRESS.matcher(this)
            .matches()
    }

    fun sharePost(): Boolean {
        Log.i(LOG_TAG,"sharePost start");
        val intent = Intent(Intent.ACTION_SENDTO).apply {
            data = Uri.parse("mailto:") // only email apps should handle this

            val postAddress = pref.getString("postAddress", "")
            if (postAddress == null) {
                Toast.makeText(context, getString(R.string.empty_email), Toast.LENGTH_LONG).show()
                return true
            }
            if (!postAddress.isEmailValid()) {
                Log.i(LOG_TAG,"Email $postAddress is invalid")
                Toast.makeText(context, getString(R.string.invalid_email), Toast.LENGTH_LONG).show()
                return true
            }

            val emails: Array<String> = arrayOf(postAddress)
            putExtra(Intent.EXTRA_EMAIL, emails)

            var defaultTags = pref.getString("defaultTags", "")
            if (!defaultTags.isNullOrEmpty()) defaultTags += ", "
            val builder = StringBuilder()
            builder.append("\nlj-tags: ")
                .append(defaultTags)
                .append(binding.tags.text.toString())
                .append("\nlj-mood: ")
                .append(binding.mood.text.toString())
                .append("\nlj-security: private\n\n")
                .append(binding.content.text.toString())
            val subject: String = binding.title.text.toString()
            putExtra(Intent.EXTRA_SUBJECT, subject)
            putExtra(Intent.EXTRA_TEXT, builder.toString())
        }
        Log.i(LOG_TAG, "posting message...")
        if (getActivity()?.let { intent.resolveActivity(it.getPackageManager()) } != null) {
            startActivity(intent)
        } else {
            Log.i(LOG_TAG, "Oops, activity not found")
        }
        return true
    }

    fun clear(): Boolean {
// https://www.journaldev.com/309/android-alert-dialog-using-kotlin
        val builder = AlertDialog.Builder(requireContext(), R.style.AlertDialogStyle)
        val clickOK = { dialog: DialogInterface, which: Int ->
            pref.edit().remove("content")
            with(binding) {
                initPost()
                tags.text.clear()
                mood.text.clear()
                title.setText(getDate())
            }
        }
        val clickCancel = { dialog: DialogInterface, which: Int ->
        }

        with(builder)
        {
            setTitle("Clear")
            setMessage("Clear the entry form?")
            setCancelable(true)
            setPositiveButton("OK", DialogInterface.OnClickListener(function = clickOK))
            setNegativeButton(
                "Cancel",
                DialogInterface.OnClickListener(function = clickCancel)
            )
            show()
        }

        return true
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.main_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        when (item.itemId) {
            R.id.settings -> openSettings()
            R.id.share -> sharePost()
            R.id.clear -> clear()
            else -> super.onOptionsItemSelected(item)
        }

    fun updateTextSize(size: Float) {
        with(binding) {
            content.setTextSize(TypedValue.COMPLEX_UNIT_SP, size)
            title.setTextSize(TypedValue.COMPLEX_UNIT_SP, size)
            tags.setTextSize(TypedValue.COMPLEX_UNIT_SP, size)
            mood.setTextSize(TypedValue.COMPLEX_UNIT_SP, size)
        }
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        if (key == "textSize") {
            var size =
                pref.getString(key, textSizeDefault.toString())?.toFloatOrNull() ?: textSizeDefault
            updateTextSize(size)
        }
    }

    override fun onPause() {
        super.onPause()
        pref.edit().putString("content", binding.content.text.toString()).apply()
    }

    override fun onResume() {
        super.onResume()
        if (pref.contains("content")) {
            binding.content.setText(pref.getString("content", ""))
        }
    }
}