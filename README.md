# About the project

**IJPost** is a simple interface for posting to LiveJournal-based blogs by email. It was created to satisfy my need for a minimalistic interface for short end-of-day personal report / gratitude journal which is convenient to read on a desktop computer as a continuous flow of text (as opposed to clickable subjects/snippets). (Other requirements: subject pre-filled with the date, dark theme, no ads, an option to update the same blog from a desktop computer. Moods are a bonus.) Why post by email? Because I don't want to muck around with their XML-RPC API. 

I needed it for [InsaneJournal](https://www.insanejournal.com/), to make some use of my permanent account, hence the name. 

It auto-fills the post subject to today's date minus 4 hours, so the entries posted after 0:00 still count as the same day. From IJ-specific metadata, it supports tags and moods. It's possible to add preset tags in the settings. The post privacy is hardcoded to private.

One setting required for the app to work is post address. You have to configure your IJ (or DW or LJ) account for posting by email (Mobile post settings) and specify that address (email address with the pin) in IJPost settings.

To post, tap the Share icon on the toolbar. Select Gmail (or any other email app), and it opens with all the fields pre-filled - just click "Send". 

The post contents are not erased automatically - use the Clear button on the toolbar.

There's an optional "template" setting, which allows to set up content to be included in every post, such as recurring questions to answer every day. An example of using a template is provided in the screenshots directory.

The apk is here: [ijpost.apk](apk/ijpost.apk).

The launcher icon source is from [iconfinder](https://www.iconfinder.com/iconsets/fliraneo), Android version created in [Android Asset Studio online](https://romannurik.github.io/AndroidAssetStudio/icons-launcher.html). Part of the code is borrowed from the [Dark Mode Preferences Tutorial](https://dev.to/aurumtechie/implement-dark-night-mode-in-an-android-app-with-a-listpreference-toggle-2k5i).
___

My blog: [Cat's Mysterious Box](http://catness.org/logbook/)



