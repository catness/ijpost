- Add to build.gradle (app):

  implementation "androidx.fragment:fragment-ktx:1.2.5"
  implementation 'androidx.preference:preference-ktx:1.1.1'
  implementation 'androidx.navigation:navigation-fragment-ktx:2.3.2'
  implementation 'androidx.navigation:navigation-ui-ktx:2.3.2'

  and databinding:

   buildFeatures {
        dataBinding true
    }

- Update  ext.kotlin_version = "1.4.21" in build.gradle (project)

- Update activity_main.xml. (Can't figure out how to auto-expand the content EditText to full height.)

- Convert activity to fragment (should've started with that!)

  - move activity_main.xml to fragment_title.xml and convert to data binding layout
  - create activity_main.xml with nav_host_fragment (copy from an example, I use AnimeBrowser as the example)
  - create TitleFragment.kt (copy the structure from an example)
  - create navigation/navigation.xml, add title fragment  
  - move the spinner stuff from MainActivity to the fragment
  - add fragment stuff to MainActivity (copy from an example)
  - change the theme from DarkActionBar to NoActionBar both versions (also night)

- Add preferences menu

  - res->xml->root_preferences.xml (copy/paste)
  - copy/paste missing strings and string arrays (can copy right into strings.xml - the original is in arrays.xml)
  - res->menu->main_menu.xml (copy/paste)
  - copy settings.xml from res/drawable and res/drawable-night
     (apparently it's ok to use the night settings for both)
  - create activity: SettingsActivity 
  - copy/paste settingsactivity from the example
  - add to manifest: android:parentActivityName=".MainActivity"
  - also add android:launchMode="singleTask" to MainActivity
  - MainActivity: copy/paste the preferences and options stuff from the example

- Change colors to the AnimeBrowser colors

  - copy colors.xml from res/values and res/values-night
  - change them in themes.xml


- Add share icon

  - add "share" button to the menu (copy from pogo_types_quiz)
  - also copy the drawable (share.xml) - the same one works for night

- Share to gmail

  - move processing options menu from MainActivity to TitleFragment
    (and change, apparently it's different when calling from a fragment)
  - change onOptionsItemSelected to a switch, add the sharePost function
  - implement sharePost (lots of work!)
  - add the posting address to preferences


